# Spring Cloud Kubernetes Service Discovery #

At this year's [Geecon 2018](https://2018.geecon.cz) Mauricio Salatino had a nice talk about [Spring Cloud Kubernetes project](https://github.com/spring-cloud/spring-cloud-kubernetes). This project has a lot of nice ideas.    
One that impressed me the most is how Spring Cloud K8s does the [Service Discovery](https://en.wikipedia.org/wiki/Service_discovery). It works according the following premises:

* In K8s cluster there is no need to have [Eureka](https://www.tutorialspoint.com/spring_boot/spring_boot_eureka_server.htm). [ETCD](https://github.com/etcd-io/etcd/blob/master/Documentation/docs.md) in K8s holds all the necessary info.
* Your application will contact K8s API server for the endpoint info by the specified [K8s service](https://kubernetes.io/docs/concepts/services-networking/service/) name.
* Returned service can be then invoked via [Feign](https://cloud.spring.io/spring-cloud-netflix/multi/multi_spring-cloud-feign.html) for example.
* To have the [DiscoveryClient](https://github.com/spring-cloud/spring-cloud-commons/blob/master/spring-cloud-commons/src/main/java/org/springframework/cloud/client/discovery/DiscoveryClient.java) functional all what you need to do    
  is to **align the Kubernetes service name with the spring.application.name property**.

Sounds nice and easy, so let's test it!

## Service discovery, same namespace different pods ##

First and obvious option.   
Let's have following Spring Cloud Application (**service2-chart**)

```java
	package com.example.service2.mvc;

	import org.springframework.web.bind.annotation.GetMapping;
	import org.springframework.web.bind.annotation.RestController;

	@RestController
	public class Controller {

    	@GetMapping(path = "/service2")
    	public String respondPoint() {
        	return "Hello I'm service2";
    	}
	}
```
Resources, notice the spring.application.name.

```
spring.application.name=service2-chart
server.port=8081
```

Now let's **build and install the application into Kubernetes cluster**.    
To make it nice and smooth I prepared [helm chart](https://docs.helm.sh/developing_charts/).

* tomask79:spring-service2 tomask79$ eval $(minikube docker-env)
* tomask79:spring-service2 tomask79$ mvn clean install 
* tomask79:spring-service2 tomask79$ helm install -n service2-chart service2-chart-0.1.0.tgz 

Helm chart should install everything, but let's check just services:

	tomask79:spring-service2 tomask79$ kubectl -n default get services
	NAME             TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)          AGE
	kubernetes       ClusterIP   10.96.0.1       <none>        443/TCP          58d
	service2-chart   NodePort    10.100.48.206   <none>        8081:30467/TCP   18s

Okay, **we aligned Kubernetes service name with spring.application.name**.    
Now let's prepare another MVC boot application **invoking service2-chart app/K8s service.**    
First, Feign stub, notice that we just need to know the K8s service name and that's it:

```java
	package com.example.service1.mvc;

	import org.springframework.cloud.openfeign.FeignClient;
	import org.springframework.web.bind.annotation.GetMapping;

	@FeignClient(name = "service2-chart")
	public interface Service2Client {

    	@GetMapping("/service2")
    	String invokeService2();
	}
```
Now invoking of the service2-chart K8s service:

```java
	package com.example.service1.mvc;

	import org.springframework.beans.factory.annotation.Autowired;
	import org.springframework.web.bind.annotation.GetMapping;
	import org.springframework.web.bind.annotation.RestController;

	@RestController	
	public class Controller {

    	@Autowired
    	private Service2Client service2Client;

    	@GetMapping(path = "/service1")
    	public String respondPoint() {
        	final String service2Output = service2Client.invokeService2();
        	return "Hello I'm Service1 ->"+service2Output;
    	}
	}
```
Everything ready, so let's test it:

* tomask79:spring-service1 tomask79$ eval $(minikube docker-env)
* tomask79:spring-service1 tomask79$ mvn clean install
* tomask79:spring-service1 tomask79$ helm install -n service1-chart service1-chart-0.1.0.tgz 

Again Helm should install everything, this is the desired output:

	tomask79:spring-service1 tomask79$ kubectl -n default get services	
	NAME             TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)          AGE
	kubernetes       ClusterIP   10.96.0.1       <none>        443/TCP          58d
	service1-chart   NodePort    10.101.7.4      <none>        8082:32391/TCP   3m
	service2-chart   NodePort    10.100.48.206   <none>        8081:30467/TCP   17h

and Helm charts installed:

	tomask79:spring-service1 tomask79$ helm list
	NAME          	REVISION	UPDATED                 	STATUS  	CHART               	APP VERSION	NAMESPACE
	service1-chart	1       	Sun Dec 23 18:03:30 2018	DEPLOYED	service1-chart-0.1.0	1.0        	default  
	service2-chart	1       	Sun Dec 23 01:05:17 2018	DEPLOYED	service2-chart-0.1.0	1.0        	default  

So now let's invoke the /service1 endpoint which invokes service2-chart K8s service:

	tomask79:spring-service1 tomask79$ minikube service service1-chart --url
	http://192.168.99.100:32391

	tomask79:spring-service1 tomask79$ curl http://192.168.99.100:32391/service1
	{"timestamp":"2018-12-23T19:57:12.020+0000",
	"status":500,"error":"Internal Server Error",
	"message":"Error creating bean with name 'ribbonLoadBalancingHttpClient' 
		defined in org.springframework.cloud.netflix.ribbon.apache.HttpClientRibbonConfiguration: 
		Unsatisfied dependency expressed through method 'ribbonLoadBalancingHttpClient' parameter 2; 
		nested exception is org.springframework.beans.factory.BeanCreationException: 
		Error creating bean with name 'ribbonLoadBalancer' 
		defined in org.springframework.cloud.netflix.ribbon.RibbonClientConfiguration: 
		Bean instantiation via factory method failed; nested exception is 
			org.springframework.beans.BeanInstantiationException: 
				Failed to instantiate [com.netflix.loadbalancer.ILoadBalancer]: 
					Factory method 'ribbonLoadBalancer' threw exception; 
					nested exception is io.fabric8.kubernetes.client.KubernetesClientException: 
					Operation: [get]  for kind: [Endpoints]  with name: [service2-chart]  in namespace: 
												[default]  failed.","path":"/service1"}

What just happended is that invoking of service2-chart K8s service has failed, because [fabric8 kubernetes client](https://github.com/fabric8io/kubernetes-client)    
which is being used by Spring Cloud K8s underneath wasn't able to retrieve the endpoints data in the default namespace.    

To understand why let's repeat basic rules about Kubernetes:

- Every process running inside of pod runs under [service account](https://kubernetes.io/docs/tasks/configure-pod-container/configure-service-account/) identity.
- If not specified otherwise every pod runs under "default" serviceaccount.
- To give the pod the priviledge of getting the K8s endpoint metadata corresponding default serviceaccount    
  needs to gain at least "view" clusterrole.

Just "view" readonly [clusterrole](https://kubernetes.io/docs/reference/access-authn-authz/rbac/#user-facing-roles) is fine:

	tomask79:spring-service1 tomask79$ kubectl create rolebinding default-view-binding --clusterrole=view --serviceaccount=default:default --namespace=default
	rolebinding.rbac.authorization.k8s.io/default-view-binding created

Now let's curl the /service1 endpoint invoking the K8s service2-chart service again:

	tomask79:spring-service1 tomask79$ curl http://192.168.99.100:32391/service1
	Hello I'm Service1 ->Hello I'm service2

Nice, we have called K8s service **within the same namespace** by knowing just it's name!

## Service discovery, different namespaces different pods ##

This time let's try to call kubernetes service living in the separated namespace    
by knowing just it's name. This is also supported with very little coding change!

First let's create new namespace called "test":

	tomask79:spring-service1 tomask79$ kubectl create namespace test

Now let's **install service2-chart into the newly namespace** (you've gotta love helm here):

	tomask79:spring-service1 tomask79$ helm install -n service2-chart --namespace test service2-chart-0.1.0.tgz 

Next step is to tell ribbon in the service1 application where to look for just installed service2-chart service.    
By default it lists endpoints in the current namespace. To tell him to search service2-chart in another namespace    
you need to add following property into application.properties:

	service2-chart.ribbon.KubernetesNamespace=test

Now let's rebuild and perform re-install of service1 application into K8s cluster, **default namespace**:

* tomask79:spring-service1 tomask79$ mvn clean install
* tomask79:spring-service1 tomask79$ helm package ./service1-chart --debug
* tomask79:spring-service1 tomask79$ helm install -n service1-chart service1-chart-0.1.0.tgz 

This is the desired output before testing it out (notice the namespaces):

	tomask79:spring-service1 tomask79$ helm list
	NAME          	REVISION	UPDATED                 	STATUS  	CHART               	APP VERSION	NAMESPACE
	service1-chart	1       	Sun Dec 23 22:45:24 2018	DEPLOYED	service1-chart-0.1.0	1.0        	default  
	service2-chart	1       	Sun Dec 23 22:09:00 2018	DEPLOYED	service2-chart-0.1.0	1.0        	test     

Okay, let's try to invoke service1-chart service which calls service2-chart in another namespace:

	tomask79:spring-service1 tomask79$ minikube service service1-chart --url
	http://192.168.99.100:30176
	tomask79:spring-service1 tomask79$ curl http://192.168.99.100:30176/service1
	{"timestamp":"2018-12-23T21:47:00.649+0000","status":500,"error":"Internal Server Error",
	"message":"Error creating bean with name 'ribbonLoadBalancingHttpClient' 
	defined in org.springframework.cloud.netflix.ribbon.apache.HttpClientRibbonConfiguration: 
		Unsatisfied dependency expressed through method 'ribbonLoadBalancingHttpClient' 
			parameter 2; nested exception is org.springframework.beans.factory.BeanCreationException: 
				Error creating bean with name 'ribbonLoadBalancer' 
					defined in org.springframework.cloud.netflix.ribbon.RibbonClientConfiguration: 
						Bean instantiation via factory method failed; 
							nested exception is org.springframework.beans.BeanInstantiationException: 
								Failed to instantiate [com.netflix.loadbalancer.ILoadBalancer]: 
								Factory method 'ribbonLoadBalancer' threw exception; 
								nested exception is io.fabric8.kubernetes.client.KubernetesClientException: 
								Failure executing: 
								GET at: 
								https://kubernetes.default.svc/api/v1/namespaces/test/endpoints/service2-chart. 
								
								Message: Forbidden!Configured service account doesn't have access. 
								Service account may have been revoked. endpoints \"service2-chart\" is forbidden: 
								User \"system:serviceaccount:default:default\" cannot get endpoints in the namespace \"test\".","path":"/service1"}tomask79:spring-service1 tomask79$ 

Well, it doesn't work because of obvious reasons. default:default serviceaccount, which is the identity of the    
service1-chart K8s service, doesn't have priviledge for getting the resource metadata from the "test" namespace.    

So let's give him cluster-wide readonly view:

	tomask79:spring-service1 tomask79$ kubectl create clusterrolebinding test-view --clusterrole=view --serviceaccount=default:default
	clusterrolebinding.rbac.authorization.k8s.io/test-view created

Now let's invoke the service1-chart again:

	tomask79:spring-service1 tomask79$ curl http://192.168.99.100:30176/service1
	Hello I'm Service1 ->Hello I'm service2

Okay, this time we have invoked kubernetes service living in the separated namespace    
by knowing just it's name. I like this approach, because you can migrate your microservice    
service discovery mechanisms to kubernetes without changing your code.

Best regards

Tomas
