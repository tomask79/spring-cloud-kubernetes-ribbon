package com.example.service1.mvc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {

    @Autowired
    private Service2Client service2Client;

    @GetMapping(path = "/service1")
    public String respondPoint() {
        final String service2Output = service2Client.invokeService2();
        return "Hello I'm Service1 ->"+service2Output;
    }
}
