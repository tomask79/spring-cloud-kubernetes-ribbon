package com.example.service2.mvc;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {

    @GetMapping(path = "/service2")
    public String respondPoint() {
        return "Hello I'm service2";
    }
}
